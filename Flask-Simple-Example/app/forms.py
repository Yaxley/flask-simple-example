# -*- coding: utf-8 -*-

from flask.ext.wtf import Form
from wtforms import StringField, IntegerField, SubmitField
from wtforms.validators import Required, Length

class PersonForm(Form):
    name = StringField('Name', validators  = [Required(), Length(2, 64)])
    age = IntegerField('Age', validators = [Required()])
    submit = SubmitField('Submit')