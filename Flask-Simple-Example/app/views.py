# -*- coding: utf-8 -*-

from flask import render_template
from sqlalchemy import desc, func
from app import app, db
from .models import Person
from .forms import PersonForm


@app.route('/', methods = ['GET', 'POST'])
def index():
    form = PersonForm()

    history = Person.query.order_by(desc(Person.time)).limit(10)
    frequency =  db.session.query(Person.age, func.count(Person.age).label('freq')).group_by(Person.age).order_by(desc('freq')).limit(10)

    if form.validate_on_submit():
        person = Person(name = form.name.data, age = form.age.data)
        db.session.add(person)
        db.session.commit()

        return render_template('index.html', name = form.name.data, age = form.age.data, history = history, frequency = frequency)

    return render_template('index.html', form = form, history = history, frequency = frequency)