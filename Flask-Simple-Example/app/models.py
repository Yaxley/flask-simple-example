# -*- coding: utf-8 -*-

import datetime
from app import db


class Person(db.Model):
    __tablename__ = 'persons'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(64), index = True)
    age = db.Column(db.Integer)
    time = db.Column(db.DateTime, default = datetime.datetime.utcnow)

    def __repr__(self):
        return '<Person {}>'.format(self.name, self.age)