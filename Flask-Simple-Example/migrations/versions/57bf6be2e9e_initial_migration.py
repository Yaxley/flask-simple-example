"""Initial migration

Revision ID: 57bf6be2e9e
Revises: None
Create Date: 2015-03-18 09:38:59.295532

"""

# revision identifiers, used by Alembic.
revision = '57bf6be2e9e'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('persons',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=64), nullable=True),
    sa.Column('time', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_persons_name'), 'persons', ['name'], unique=False)
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_persons_name'), table_name='persons')
    op.drop_table('persons')
    ### end Alembic commands ###
